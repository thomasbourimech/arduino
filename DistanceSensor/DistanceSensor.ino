#include <Servo.h>

const int thresh = 20;
int angle;
long duree, distance;
Servo myServo;

void setup()
{
  pinMode(4, OUTPUT);
  pinMode(2, INPUT);
  Serial.begin(9600);
  myServo.attach(9);
}

void loop()
{
  digitalWrite(4, LOW);
  delayMicroseconds(5);

  digitalWrite(4, HIGH);
  delayMicroseconds(10);
  digitalWrite(4, LOW);

  duree = pulseIn(2, HIGH);
  Serial.println(duree);
  // distance = duree*340/(2 * 10000);
  distance = duree * 0.017;
  Serial.print("Distance: ");
  Serial.print(distance);
  Serial.println(" cm");

  if (distance <= thresh)
  {
    angle = 90;
  }
  else
  {
    angle = 0;
  }
  myServo.write(angle);
  delay(1000);
}
