int led = 8;
int Duree;

void setup()
{
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(led, OUTPUT);
  pinMode(A5, INPUT);
}

void loop()
{
  // put your main code here, to run repeatedly:
  digitalWrite(led, HIGH);
  Duree = analogRead(A5);
  Duree = map(Duree, 0, 1023, 0, 5000);
  Serial.println(Duree);
  digitalWrite(led, HIGH);
  delay(Duree / 2);
  digitalWrite(led, LOW);
  delay(Duree / 2);
}
